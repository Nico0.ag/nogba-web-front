import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { TopBarComponent } from './components/top-bar/top-bar.component'; 
import { UsersComponent } from './components/users/users.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UserNewComponent } from './components/user-new/user-new.component'; 
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { HomeComponent } from './components/home/home.component';
import { VentaComponent } from './components/venta/venta.component';
import { ModalProductosComponent } from './components/modal-productos/modal-productos.component';
import { TrabajosComponent } from './components/trabajos/trabajos.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { GastosComponent } from './components/gastos/gastos.component';
import { UservendorComponent } from './components/uservendor/uservendor.component';
import { AbmclientesComponent } from './components/abmclientes/abmclientes.component';
import { AbmvendedoresComponent } from './components/abmvendedores/abmvendedores.component';
import { AbmtrabajosComponent } from './components/abmtrabajos/abmtrabajos.component';
import { AbmproductosComponent } from './components/abmproductos/abmproductos.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavBarComponent,
    TopBarComponent, 
    UsersComponent,
    NotFoundComponent,
    UserNewComponent,  
    ForgotPasswordComponent,
    HomeComponent,
    VentaComponent,
    ModalProductosComponent,
    TrabajosComponent,
    GastosComponent,
    UservendorComponent,
    AbmclientesComponent,
    AbmvendedoresComponent,
    AbmtrabajosComponent,
    AbmproductosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
