export class VentaTrabajo {
    idcliente: number
    precio: number
    trabajos: Trabajo[]
  }
  
  export class Trabajo {
    idtrabajo: number
    cantidad: number
  }
  