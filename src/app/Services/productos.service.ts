import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Productos } from '../Interfaces/Productos';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
private url = 'http://localhost:5000/api/productos/getallprodu';
private urlpost = 'http://localhost:5000/api/productos/nuevoproducto';
private urldel = 'http://localhost:5000/api/productos/';
  constructor(private http:HttpClient) { }
 

  GetProductos(): Observable<Productos[]>{      
    return  this.http.get<Productos[]>(this.url);
  }

  PostProductos(form) {
    return this.http.post<object>(this.urlpost, form);
  } 

  DeleteProdu(id : number){
    return this.http.delete(this.urldel + id)
  }

}
