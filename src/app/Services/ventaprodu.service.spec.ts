import { TestBed } from '@angular/core/testing';

import { VentaproduService } from './ventaprodu.service';

describe('VentaproduService', () => {
  let service: VentaproduService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VentaproduService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
