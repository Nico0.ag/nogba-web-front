import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toast: ToastrService) { }

  ShowAlertError(message, title){  
  
  this.toast.error(message,title)
  }

  ShowAlertOK(message,title){      
    this.toast.success(message,title)
  
  }
  



}
