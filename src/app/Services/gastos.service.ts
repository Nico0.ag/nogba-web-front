import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GastosService {
  private url = 'http://localhost:5000/api/gastos/postgastos'
  constructor(private http:HttpClient){ } 
  
  Postgastos(gastos){
  return this.http.post<object>(this.url,gastos);    
  }  


}
