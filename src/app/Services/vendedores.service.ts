import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Vendedores } from '../Interfaces/Vendedores';

@Injectable({
  providedIn: 'root'
})
export class VendedoresService {

  private url = 'http://localhost:5000/api/vendedores/getallvendedores';

  constructor(private http:HttpClient) { }

  GetVendedores(): Observable<Vendedores[]>{      
    return  this.http.get<Vendedores[]>(this.url);     
    }

}
