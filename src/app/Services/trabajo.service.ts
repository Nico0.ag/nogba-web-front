import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Trabajos } from '../Interfaces/Trabajos';
@Injectable({
  providedIn: 'root'
})
export class TrabajoService {

  private url = 'http://localhost:5000/api/trabajos/getalltra';

  constructor(private http:HttpClient) { }

  GetTrabajos(): Observable<Trabajos[]>{      
    return  this.http.get<Trabajos[]>(this.url);     
    }
}
