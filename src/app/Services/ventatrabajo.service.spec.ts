import { TestBed } from '@angular/core/testing';

import { VentatrabajoService } from './ventatrabajo.service';

describe('VentatrabajoService', () => {
  let service: VentatrabajoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VentatrabajoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
