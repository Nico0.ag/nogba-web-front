import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Clientes } from '../Interfaces/Clientes';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  private url = 'http://localhost:5000/api/clientes/getallcli';
  private urlpost = 'http://localhost:5000/api/clientes/nuevocliente';

  constructor(private http:HttpClient) { }
  
  PostClientes(form) {
    return this.http.post<object>(this.urlpost, form);} 

  GetClientes(): Observable<Clientes[]>{      
    return  this.http.get<Clientes[]>(this.url);     
    }
}
