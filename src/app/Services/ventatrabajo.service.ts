import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VentaTrabajo } from '../Interfaces/VentaTrabajo';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VentatrabajoService {

  private url = 'http://localhost:5000/api/ventatrabajo/venta';
  constructor(private http:HttpClient) { }

  GetVenTra(): Observable<VentaTrabajo[]>{      
    return  this.http.get<VentaTrabajo[]>(this.url);}

  PostVenTra(form) {
    return this.http.post<object>(this.url, form);} 
}
