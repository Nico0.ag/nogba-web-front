import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
private subject = new Subject<any>();

  constructor() { }

  EnviarModal(){
    this.subject.next();
  }
  
  ObtenerModal(): Observable<any>{ 
    return this.subject.asObservable();
  }
}
