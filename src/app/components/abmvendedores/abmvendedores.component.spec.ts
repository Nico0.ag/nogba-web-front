import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbmvendedoresComponent } from './abmvendedores.component';

describe('AbmvendedoresComponent', () => {
  let component: AbmvendedoresComponent;
  let fixture: ComponentFixture<AbmvendedoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbmvendedoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbmvendedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
