import { Component, OnInit } from '@angular/core';
import { Vendedores } from 'src/app/Interfaces/Vendedores';
import { GastosService } from 'src/app/Services/gastos.service';
import { VendedoresService } from 'src/app/Services/vendedores.service';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { Gastos } from 'src/app/Interfaces/Gastos';
import { ToastService } from 'src/app/Services/toastr.service';

@Component({
  selector: 'app-gastos',
  templateUrl: './gastos.component.html',
  styleUrls: ['./gastos.component.scss']
})
export class GastosComponent implements OnInit {
 ven : Vendedores[]
 vender : string
 envio : Gastos ={ descripcion :"", valor :0, vendedor :"" }
 message: any;
 constructor(private venservice:VendedoresService,
             private toastr: ToastService,
             private gasto: GastosService){ 
 this.venservice.GetVendedores().subscribe(data =>(this.ven = data))  
 }

 ngOnInit(): void {
 }

 Enviar(f:NgForm){
  var datos ={
     valor: Number(this.envio.valor),
     vendedor:this.vender,
     descripcion:this.envio.descripcion
   }    
   if (this.envio.descripcion != "" && this.envio.valor != 0 && f.valid){ //f.valid valida el combobox
    Swal.fire({
      title: 'Confirmar Venta?',
      icon: 'info',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar',        
      reverseButtons: true
    }).then((result) =>{ //RESOLVER ACA, IF PARA CONFIRMAR VENTA 
      if (result.value){         
          this.gasto.Postgastos(datos).subscribe((resultado) =>{
          console.log(resultado)
        if(resultado==null){
          Swal.fire(
            'Realizado!',
            'Venta realizada correctamente',
            'success')  
            setTimeout(function(){
              window.location.reload(); //RECARGA LA PAGINA LUEGO DE 5 SEGUNDOS BORRA TODOS LOS DATOS "REFRESH"
           }, 5000);
          }},(error) =>{             //error de server o falta de seleccion combobox
            Swal.fire(
              'Error!',
              'No se pudo realizar la venta',
              'error')
          });                         
        }
    })
  }//aca termina el primer if para validar todos los datos 
  else
  {
  this.message = this.toastr.ShowAlertError('Faltan datos para finalizar','Advertencia!');
  }          
 
   
 }

}
