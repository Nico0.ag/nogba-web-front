import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'; 
import { Clientes } from 'src/app/Interfaces/Clientes';
import { ClientesService } from 'src/app/Services/clientes.service';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { ToastService } from 'src/app/Services/toastr.service';
import { ModalService } from 'src/app/Services/modal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-uservendor',
  templateUrl: './uservendor.component.html',
  styleUrls: ['./uservendor.component.scss']
})

export class UservendorComponent implements OnInit {
message :any
datos : Clientes = {nombreApe: "", telefono: null};
modalservicesuscription: Subscription; //rxjs

  constructor(private modal: NgbModal,
              private modalservice: ModalService,
              private toastr: ToastService,
              private envio: ClientesService){
              this.modalservicesuscription = 
              this.modalservice.ObtenerModal().subscribe(() =>{
              
              })
  }

  ngOnInit(): void {
  }
 
  Ingreso(abrime){
    this.modal.open(abrime,{size:'sm'});
  }
 
  Enviar(f:NgForm){
    console.log(this.datos)
    if (this.datos.nombreApe != "" && this.datos.telefono != 0 && this.datos.telefono != null){ //f.valid valida el combobox!
      Swal.fire({
        title: 'Confirmar Venta?',
        icon: 'info',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Aceptar',        
        reverseButtons: true
      }).then((result) =>{ //RESOLVER ACA, IF PARA CONFIRMAR VENTA 
        if (result.value){         
          this.envio.PostClientes(this.datos).subscribe((resultado) =>{
            console.log(resultado)          
          if(resultado==null){
            Swal.fire(
              'Realizado!',
              'Venta realizada correctamente',
              'success')  
              setTimeout(function(){
                window.location.reload(); //RECARGA LA PAGINA LUEGO DE 5 SEGUNDOS BORRA TODOS LOS DATOS "REFRESH"
             }, 5000);
            }},(error) =>{             //error de server o falta de seleccion combobox
              Swal.fire(
                'Error!',
                'No se pudo realizar la venta',
                'error')
            });                         
          }
      })
    }//aca termina el primer if para validar todos los datos 
    else
    {
    this.message = this.toastr.ShowAlertError('Faltan datos para finalizar','Advertencia!');
    }          
  
    
   /*
    
    */
    
     
  }

}
