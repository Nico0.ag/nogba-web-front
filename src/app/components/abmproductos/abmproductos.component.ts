import { Component, OnInit } from '@angular/core';
import { Productos } from 'src/app/Interfaces/Productos';
import { ProductosService } from 'src/app/Services/productos.service';

@Component({
  selector: 'app-abmproductos',
  templateUrl: './abmproductos.component.html',
  styleUrls: ['./abmproductos.component.scss']
})
export class AbmproductosComponent implements OnInit {
producto : Productos[]
  constructor(private produ: ProductosService){        
    this.produ.GetProductos().subscribe(data =>(this.producto = data))
  };

  DeleteProdu(id, i){  
    this.producto.splice(i, 1,)  
    this.produ.DeleteProdu(id).subscribe((resultado)=>{
      console.log(resultado)
    })
  };

  ngOnInit(): void {
  }

}
