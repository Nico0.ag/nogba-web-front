import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbmtrabajosComponent } from './abmtrabajos.component';

describe('AbmtrabajosComponent', () => {
  let component: AbmtrabajosComponent;
  let fixture: ComponentFixture<AbmtrabajosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbmtrabajosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbmtrabajosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
