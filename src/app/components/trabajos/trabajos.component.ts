import { Component, OnInit } from '@angular/core';
import { Clientes } from 'src/app/Interfaces/Clientes';
import { Trabajos } from 'src/app/Interfaces/Trabajos'; 
import { ClientesService } from 'src/app/Services/clientes.service'; 
import { TrabajoService } from 'src/app/Services/trabajo.service'; 
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { ToastService } from 'src/app/Services/toastr.service';
import { VentaTrabajo } from 'src/app/Interfaces/VentaTrabajo';
import { VentatrabajoService } from 'src/app/Services/ventatrabajo.service';
import { ModalService } from 'src/app/Services/modal.service';

@Component({
  selector: 'app-trabajos',
  templateUrl: './trabajos.component.html',
  styleUrls: ['./trabajos.component.scss']
})
export class TrabajosComponent implements OnInit {
  client: Clientes[]
  trabajo: Trabajos[]
  tipoDeTrabajo: string;
  nombre : string;
  cartTotal = 0
  cliente = 0  
  carrito = []
  datos = []
  message : any
  
 constructor(private traba: TrabajoService,
             private ventaprodu: VentatrabajoService,
             private modalservice: ModalService,
             private toastr: ToastService,
             private clie:  ClientesService){     
   this.traba.GetTrabajos().subscribe(data =>(this.trabajo = data))
   this.clie.GetClientes().subscribe(data =>(this.client = data))     
 };  

 AbrirModal(){
  this.modalservice.EnviarModal();
  console.log("me abri")
 }

 GetTrabaResp(){ //get responsive
  this.traba.GetTrabajos().subscribe((Response) =>{
  this.trabajo = Response;})
 }

 Calculo(){ //cantidad por precio = total
  this.cartTotal =0 
  this.carrito.forEach(item => {
  this.cartTotal += (item.cantidad * item.precio)})
 }

 SearchTra(){  
   if(this.nombre ! ==""){
     this.GetTrabaResp();
   }else{
     this.trabajo = this.trabajo.filter(res =>{
   return res.tipoDeTrabajo.toLocaleLowerCase
   ().match(this.nombre.toLocaleLowerCase());
 });}}

 addTrabToCart(trabajo: Trabajos){
  let productExists = false  
  for (let a in  this.datos) {// utilizo este for para cargar datos en un segundo carrito
    if (this.datos[a].trabajoID === trabajo.idTrabajos) { //si hay match => cantidad++ debo agregar datos[a]
      this.datos[a].cantidad++
      productExists = true
      break;
    }}      
  for (let i in this.carrito) { //carrito que se muestra en pantalla
    if (this.carrito[i].trabajoID === trabajo.idTrabajos) { //si hay match => cantidad++ debo agregar datos[a]
      this.carrito[i].cantidad++
      productExists = true
      break;
    }}
  if (!productExists){ //existe producto entonces haces el push
    this.carrito.push({ //carrito en pantalla
      trabajoID: trabajo.idTrabajos,
      descripcion: trabajo.tipoDeTrabajo,
      cantidad: 1,
      precio: trabajo.valor, 
    })       
    this.datos.push({ //datos para el json
      idtrabajo: trabajo.idTrabajos,       
      cantidad: 1
    })                     
  }            
 
  
 }

 DeleteCarrito(id, i){  
 this.carrito.splice(i, 1,)     
 }
  
 ngOnInit(): void {

 }

 PostVentaTra(f:NgForm){ 
    this.Calculo();      
    var enviar : VentaTrabajo ={ //faltan datos acaaaaaaa
      idcliente : Number(this.cliente), //el dato llega en string, number(this.dato) lo recibe tipo number! funciona!
      precio: this.cartTotal,
      trabajos: this.datos
      };   
      console.log(enviar)
    if (this.cartTotal > 0 && f.valid){ //valida el dato cliente del html  
      Swal.fire({
        title: 'Confirmar Venta?',
        icon: 'info',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Aceptar',        
        reverseButtons: true
      }).then((result) =>{ //RESOLVER ACA, IF PARA CONFIRMAR VENTA 
        if (result.value){         
            this.ventaprodu.PostVenTra(enviar).subscribe((resultado) =>{ //fijate traer el service
            console.log(resultado)
            
          if(resultado==null){
            Swal.fire(
              'Realizado!',
              'Venta realizada correctamente',
              'success')  
              setTimeout(function(){
                window.location.reload(); //RECARGA LA PAGINA LUEGO DE 5 SEGUNDOS BORRA TODOS LOS DATOS REFRESH
             }, 5000);
            }},(error) =>{             //error de server o falta de seleccion combobox
              Swal.fire(
                'Error!',
                'No se pudo realizar la venta',
                'error')
            });                         
          }
      })
    }//aca termina el primer if para validar todos los datos 
    else
    {
    this.message = this.toastr.ShowAlertError('Faltan datos para finalizar','Advertencia!');
    }  
 }

}
