import { Component, OnInit } from '@angular/core';
import { Clientes } from 'src/app/Interfaces/Clientes';
import { Productos } from 'src/app/Interfaces/Productos';
import { VentaProducto } from 'src/app/Interfaces/VentaProducto';
import { ClientesService } from 'src/app/Services/clientes.service';
import { ProductosService } from 'src/app/Services/productos.service'; 
import { ToastService } from 'src/app/Services/toastr.service';
import { VentaproduService } from 'src/app/Services/ventaprodu.service';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.scss']
})
export class VentaComponent implements OnInit {
  client: Clientes[]     //para el html combobox
  producto: Productos[]  //para la tabla del html
  nombre: string;        //para el buscador responsive
  cartTotal = 0
  carrito =[]             
  datos =[]
  cliente =0       //recibe el dato del html, el idcliente     
  message : any;         //lo uso para el toast
  constructor(private toastr: ToastService,
              private clie: ClientesService,
              private ventprod : VentaproduService,
              private produ: ProductosService){   
  this.clie.GetClientes().subscribe(data =>(this.client = data))
  this.produ.GetProductos().subscribe(data =>(this.producto = data))
  };    

  ngOnInit(): void{
  }

  Calculo(){ //cantidad por precio = total
    this.cartTotal =0 
    this.carrito.forEach(item => {
    this.cartTotal += (item.cantidad * item.precio)     
    })    
  }

  GetProduResp(){ //get responsive
    this.produ.GetProductos().subscribe((Response) =>{
    this.producto = Response;})
  }

  SearchProdu(){ //busca producto por nombre, luego llama a getproduresp() y hace un get
   if(this.nombre ! == ""){
     this.GetProduResp();
   }else{
   this.producto = this.producto.filter(res =>{
   return res.nombre.toLocaleLowerCase
   ().match(this.nombre.toLocaleLowerCase());
  });}}  

  DeleteCarrito(i){  
    this.carrito.splice(i,1) 
  }    

  addProductToCart(product: Productos){
    let productExists = false // producto en carrito no existe
    for (let a in  this.datos) {// utilizo este for para cargar datos en un segundo carrito
      if (this.datos[a].productID === product.id_Productos) { //si hay match => cantidad++ debo agregar datos[a]
        this.datos[a].cantidad++
        productExists = true
        break;
      }}      
    for (let i in this.carrito) { //carrito que se muestra en pantalla
      if (this.carrito[i].productID === product.id_Productos) { //si hay match => cantidad++ debo agregar datos[a]
        this.carrito[i].cantidad++
        productExists = true
        break;
      }}
    if (!productExists){ //existe producto entonces haces el push
      this.carrito.push({ //carrito en pantalla
        productID: product.id_Productos,
        descripcion: product.nombre,
        cantidad: 1,
        precio: product.valor, 
      })       
      this.datos.push({ //datos para el json
        idproducto: product.id_Productos,       
        cantidad: 1
      })    
    }          
  }   

  PostVentaProdu(f: NgForm){  
    this.Calculo();      
    var enviar : VentaProducto ={
      idcliente : Number(this.cliente), //el dato llega en string, number(this.dato) lo recibe tipo number! funciona!
      precio: this.cartTotal,
      productos: this.datos
      };   
    if (this.cartTotal > 0 && f.valid){ //valida el dato cliente del html  
      Swal.fire({
        title: 'Confirmar Venta?',
        icon: 'info',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Aceptar',        
        reverseButtons: true
      }).then((result) =>{ //RESOLVER ACA, IF PARA CONFIRMAR VENTA 
        if (result.value){         
            this.ventprod.Postventprodu(enviar).subscribe((resultado) =>{
            console.log(resultado)
            
          if(resultado==null){
            Swal.fire(
              'Realizado!',
              'Venta realizada correctamente',
              'success')  
              setTimeout(function(){
                window.location.reload(); //RECARGA LA PAGINA LUEGO DE 5 SEGUNDOS BORRA TODOS LOS DATOS REFRESH
             }, 5000);
            }},(error) =>{             //error de server o falta de seleccion combobox
              Swal.fire(
                'Error!',
                'No se pudo realizar la venta',
                'error')
            });                         
          }
      })
    }//aca termina el primer if para validar todos los datos 
    else
    {
    this.message = this.toastr.ShowAlertError('Faltan datos para finalizar','Advertencia!');
    }          
  }    

}