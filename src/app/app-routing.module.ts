import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';  
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UserNewComponent } from './components/user-new/user-new.component'; 
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { HomeComponent } from './components/home/home.component';
import { VentaComponent } from './components/venta/venta.component';
import { TrabajosComponent } from './components/trabajos/trabajos.component';
import { GastosComponent } from './components/gastos/gastos.component';
import { UservendorComponent } from './components/uservendor/uservendor.component';
import { AbmclientesComponent } from './components/abmclientes/abmclientes.component';
import { AbmproductosComponent } from './components/abmproductos/abmproductos.component';
import { AbmtrabajosComponent } from './components/abmtrabajos/abmtrabajos.component';
import { AbmvendedoresComponent } from './components/abmvendedores/abmvendedores.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'trabajos', component: TrabajosComponent },
  { path: 'users', component: UsersComponent },
  { path: 'venta', component: VentaComponent },
  { path: 'user-new', component: UserNewComponent }, 
  { path: 'not-found', component: NotFoundComponent },
  { path: 'gastos', component: GastosComponent },
  { path: 'uservendor', component: UservendorComponent },
  { path: 'abmclientes', component: AbmclientesComponent},
  { path: 'abmproductos', component: AbmproductosComponent},
  { path: 'abmtrabajos', component: AbmtrabajosComponent},
  { path: 'abmvendedores', component: AbmvendedoresComponent},
  { path: '**', component: HomeComponent } //principal eliminar map component// poner ultimas ventas ;)
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
