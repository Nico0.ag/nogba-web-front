# Evim.Front

Este proyecto fué generado usando el [Angular CLI](https://github.com/angular/angular-cli) versión 10.2.2.

## Server de desarrollo

Correr `ng serve` para el servidor de desarrollo. Luego abrir `http://localhost:4200/`. La aplicación cambiará si tocamos cualquiera de los fuentes.

## Code scaffolding

Ejecutar `ng generate component nombre-del-componente` para generar un nuevo componente. Tambien se puede usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Ejecutar `ng build` para generar el proyecto. El sitio generado se guardará en la carpeta `dist/`. Usar la opción `--prod` para generar un build de producción.

## Ejecutar los unit tests

Ejecutar `ng test` para ejecurar los tests usando [Karma](https://karma-runner.github.io).

## Ejecutar los end-to-end tests

Ejecutar `ng e2e` para ejecutar los end-to-end tests usando [Protractor](http://www.protractortest.org/).

## Obtener ayuda

Para obtener ayuda del Angular CLI usar `ng help` o visitar la página [Angular CLI Overview and Command Reference](https://angular.io/cli).

## Plug-ins de VS Code utilizados

Si se utiliza VS Code para el desarrollo, es recomendable instalar los siguientes plugins:

- Angular Language Service 
- Angular 10 Snippets 
- Bootstrap 4, Font awesome 4, Font Awesome 5 Free & Pro snippets
- HTML CSS Support
- JSON to TS
- Material Icon Theme
- Prettier - Code formatter
- PWA Tools
- Terminal
- TSLint
- TypeScript Importer

